// Visual Studio solution: x64, no precompiled headers, -MT, MinHook https://github.com/TsudaKageyu/minhook/issues/62
#undef UNICODE
#include "MinHook.h"
#include <Windows.h>
#include <iostream>
#include <fstream>
#include <string>

#if defined _M_X64
#pragma comment(lib, "libMinHook.x64.lib")
#elif defined _M_IX86
#pragma comment(lib, "libMinHook.x86.lib")
#endif

WNDPROC wndProc = NULL;
void (*updateCrosshair)(__int64, __int64, float*, float*, float, float) = NULL;

float sens = 1.0f;
float yaw = 0.022f;
float pitch = 0.022f;
float xDegreesFor1Dot = 0;
float yDegreesFor1Dot = 0;
float deltaX = 0;
float deltaY = 0;

const WORD num1 = 0x4f;
const WORD num2 = 0x50;
const WORD num3 = 0x51;
const WORD num4 = 0x4b;

RAWINPUT* rawinputBuffer = (RAWINPUT*) new BYTE[2048]; // allocate byte array for raw input struct and assume 2048 btyes is enough
UINT rawinputBufferSize = 2048;

LRESULT CALLBACK myWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
	case WM_INPUT: {
		// copy data into raw input structure
		if (GetRawInputData((HRAWINPUT)lParam, RID_INPUT, rawinputBuffer, &rawinputBufferSize, sizeof(RAWINPUTHEADER)) <= 0) {
			return CallWindowProc(wndProc, hWnd, message, wParam, lParam);
		}

		if (rawinputBuffer->header.dwType == RIM_TYPEMOUSE) {
			deltaX -= rawinputBuffer->data.mouse.lLastX * xDegreesFor1Dot;
			deltaY += rawinputBuffer->data.mouse.lLastY * yDegreesFor1Dot;

			if (rawinputBuffer->data.mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_DOWN) {
				INPUT input[1];
				input[0].type = INPUT_KEYBOARD; input[0].ki.wScan = num1; input[0].ki.dwFlags = KEYEVENTF_SCANCODE; input[0].ki.time = 0; input[0].ki.dwExtraInfo = 0;
				SendInput(1, input, sizeof(input[0]));
			}

			if (rawinputBuffer->data.mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_UP) {
				INPUT input[1];
				input[0].type = INPUT_KEYBOARD; input[0].ki.wScan = num1; input[0].ki.dwFlags = KEYEVENTF_SCANCODE | KEYEVENTF_KEYUP; input[0].ki.time = 0; input[0].ki.dwExtraInfo = 0;
				SendInput(1, input, sizeof(input[0]));
			}

			if (rawinputBuffer->data.mouse.usButtonFlags & RI_MOUSE_RIGHT_BUTTON_DOWN) {
				INPUT input[1];
				input[0].type = INPUT_KEYBOARD; input[0].ki.wScan = num2; input[0].ki.dwFlags = KEYEVENTF_SCANCODE; input[0].ki.time = 0; input[0].ki.dwExtraInfo = 0;
				SendInput(1, input, sizeof(input[0]));
			}

			if (rawinputBuffer->data.mouse.usButtonFlags & RI_MOUSE_RIGHT_BUTTON_UP) {
				INPUT input[1];
				input[0].type = INPUT_KEYBOARD; input[0].ki.wScan = num2; input[0].ki.dwFlags = KEYEVENTF_SCANCODE | KEYEVENTF_KEYUP; input[0].ki.time = 0; input[0].ki.dwExtraInfo = 0;
				SendInput(1, input, sizeof(input[0]));
			}
		}

		break;
	}
	}

	return CallWindowProc(wndProc, hWnd, message, wParam, lParam);
}

void myUpdateCrosshair(__int64 a1, __int64 a5, float* deltaYDegrees, float* deltaXDegrees, float a4, float a6) {
	*deltaXDegrees = deltaX;
	deltaX = 0;
	*deltaYDegrees = deltaY;
	deltaY = 0;
}

void doHooks() {
	if (MH_Initialize() != MH_OK) {
		OutputDebugString("Failed to start MinHook");
		return;
	}

	BYTE* updateCrosshairAddress = ((BYTE*)GetModuleHandle(NULL)) + 0x1C9760; // PTS
	// BYTE* updateCrosshairAddress = ((BYTE*)GetModuleHandle(NULL)) + 0x1C94B0; // LIVE
	// find by break pointing reads to sens & byte array pattern search

	if (MH_CreateHook(updateCrosshairAddress, &myUpdateCrosshair, reinterpret_cast<LPVOID*>(&updateCrosshair)) != MH_OK) {
		OutputDebugString("Failed to create hook");
		return;
	}

	if (MH_EnableHook(updateCrosshairAddress) != MH_OK) {
		OutputDebugString("Failed to enable hook");
		return;
	}

	HWND hWnd = FindWindow("Lovecraft", NULL); // alternatively, enumerate windows by process or get it from WinMain's call to CreateWindowEx
	// Message-only window with title "Client_MessageWindow" is not used in game
	if (!hWnd) {
		OutputDebugString("Couldn't find window handle");
		return;
	}
	wndProc = (WNDPROC)SetWindowLongPtr(hWnd, GWLP_WNDPROC, (LONG_PTR)myWndProc); // hook can be replaced with GetMessage loop for raw input messages
}

void registerMouseForRawInput(bool unregister) {
	RAWINPUTDEVICE Rid[1];

	Rid[0].usUsagePage = 0x01;
	Rid[0].usUsage = 0x02;
	Rid[0].dwFlags = unregister ? RIDEV_REMOVE : 0; // RIDEV_NOLEGACY breaks raw input
	Rid[0].hwndTarget = NULL;

	if (!RegisterRawInputDevices(Rid, 1, sizeof(Rid[0]))) {
		OutputDebugString("RegisterRawInputDevices failed");
	}
}

void start() {
	std::string line;
	std::ifstream file("sens.txt");
	if (file.is_open()) {
		getline(file, line,',');
		sens = std::stof(line, NULL);
		getline(file, line, ',');
		yaw = std::stof(line, NULL);
		getline(file, line, ',');
		pitch = std::stof(line, NULL);
		file.close();
	} else {
		OutputDebugString("Unable to open sens.txt");
	}

	xDegreesFor1Dot = sens * yaw;
	yDegreesFor1Dot = sens * pitch;

	doHooks();
	registerMouseForRawInput(false);

	/*** This is a bad hack designed to fix the fact that dinput steals mouse control on alt-tab ***/
	if (!RegisterHotKey(NULL, 1, 0, VK_SPACE)) {
		OutputDebugString("Register hotkey failed");
	}
	MSG msg = { 0 };
	while (GetMessage(&msg, NULL, 0, 0) != 0) {
		if (msg.message == WM_HOTKEY && msg.wParam == 1) {
			registerMouseForRawInput(true);
			registerMouseForRawInput(false);
		}
	}

	delete[] rawinputBuffer;
}

BOOL WINAPI DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved) {
	switch (ul_reason_for_call) {
	case DLL_PROCESS_ATTACH: {
		DisableThreadLibraryCalls(hModule);
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)&start, 0, 0, 0);
		break;
	}

	case DLL_THREAD_ATTACH: {
		break;
	}

	case DLL_THREAD_DETACH: {
		break;
	}

	case DLL_PROCESS_DETACH: {
		break;
	}
	}
	return TRUE;
}